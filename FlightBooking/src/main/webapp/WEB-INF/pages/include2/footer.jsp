<%-- 
    Document   : footer
    Created on : Nov 21, 2020, 2:47:26 PM
    Author     : huythang
--%>

<footer class="row tm-mt-big">
    <div class="col-12 font-weight-light text-center">
        <p class="d-inline-block tm-bg-black text-white py-2 px-4">
            Copyright &copy; 2018 Admin Dashboard . Created by
            <a rel="nofollow" href="https://www.tooplate.com" class="text-white tm-footer-link">Tooplate</a>
        </p>
    </div>
</footer>
