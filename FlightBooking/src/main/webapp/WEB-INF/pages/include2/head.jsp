<%-- 
    Document   : head
    Created on : Nov 21, 2020, 2:44:53 PM
    Author     : huythang
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Dashboard Admin Template by Tooplate.com</title>
<!--

Template 2108 Dashboard

    http://www.tooplate.com/view/2108-dashboard

-->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600">
<!-- https://fonts.google.com/specimen/Open+Sans -->
<link rel="stylesheet" href="<c:url value="/resourcesAdmin/css/fontawesome.min.css"/>">
<!-- https://fontawesome.com/ -->
<link rel="stylesheet" href="<c:url value="/resourcesAdmin/css/fullcalendar.min.css"/>">
<!-- https://fullcalendar.io/ -->
<link rel="stylesheet" href="<c:url value="/resourcesAdmin/css/bootstrap.min.css"/>">
<!-- https://getbootstrap.com/ -->
<link rel="stylesheet" href="<c:url value="/resourcesAdmin/css/tooplate.css"/>">
