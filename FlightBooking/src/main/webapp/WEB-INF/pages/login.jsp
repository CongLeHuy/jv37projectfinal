<%-- 
    Document   : login
    Created on : Jun 20, 2019, 8:17:26 PM
    Author     : AnhLe
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="include2/head.jsp"/>
    </head>
    <body class="bg03">
        <div class="container">
            <div class="row tm-mt-big">
                <div class="col-12 mx-auto tm-login-col">
                    <div class="bg-white tm-block">
                        <div class="row">
                            <div class="col-12 text-center">
                                <i class="fas fa-3x fa-tachometer-alt tm-site-icon text-center"></i>
                                <h2 class="tm-block-title mt-3">Login</h2>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-12">
                                <form action="<c:url value="j_spring_security_check"/>" method="post" class="tm-login-form">
                                    <div class="input-group">
                                        <label for="username" class="col-xl-4 col-lg-4 col-md-4 col-sm-5 col-form-label">Username</label>
                                        <input name="username" type="email" class="form-control validate col-xl-9 col-lg-8 col-md-8 col-sm-7" id="username" required>
                                    </div>
                                    <div class="input-group mt-3">
                                        <label for="password" class="col-xl-4 col-lg-4 col-md-4 col-sm-5 col-form-label">Password</label>
                                        <input name="password" type="password" class="form-control validate" id="password" required>
                                    </div>
                                    <c:if test="${message != null && message != ''}">
                                        <p style="color: red; text-align: center;">${message}</p>
                                    </c:if>
                                    <div class="input-group mt-3">
                                        <button type="submit" class="btn btn-primary d-inline-block mx-auto">Login</button>
                                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                    </div>
                                    <div class="input-group mt-3">
                                        <p><em>Just put a character to login.</em></p>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <jsp:include page="include2/footer.jsp"/>
        </div>
    </body>
</html>
