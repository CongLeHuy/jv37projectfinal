<%-- 
    Document   : register
    Created on : Nov 21, 2020, 8:46:07 AM
    Author     : huythang
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="register-form">
                        <h3 class="billing-title text-center">Register</h3>
                        <p class="text-center mt-40 mb-30">Create your very own account </p>
                        <form action="register" method="POST">
                            <input type="text" placeholder="Full name*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Full name*'" required="" class="common-input mt-20">
                            <input type="email" placeholder="Email address*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email address*'" required="" class="common-input mt-20">
                            <input type="password" placeholder="Password*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Password*'" required="" class="common-input mt-20">
                            <input type="text" placeholder="Phone number*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Phone number*'" required="" class="common-input mt-20">
                            <input type="date" placeholder="Brith Date*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Brith Date*'" required="" class="common-input mt-20">
                            <input type="radio" placeholder="Gender*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Gender*'" required="" class="common-input mt-20">
                            <input type="text" placeholder="Address*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Address*'" required="" class="common-input mt-20">
                            <input type="number" placeholder="IdCard*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'IdCard*'" required="" class="common-input mt-20">
                            <button class="view-btn color-2 mt-20 w-100"><span>Register</span></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
