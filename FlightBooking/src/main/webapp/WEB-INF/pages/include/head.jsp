<%-- 
    Document   : head
    Created on : Nov 21, 2020, 9:51:12 AM
    Author     : huythang
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Favicon-->
<link rel="shortcut icon" href="img/fav.png">
<!-- Author Meta -->
<meta name="author" content="CodePixar">
<!-- Meta Description -->
<meta name="description" content="">
<!-- Meta Keyword -->
<meta name="keywords" content="">
<!-- meta character set -->
<meta charset="UTF-8">
<!-- Site Title -->
<title>Flight</title>

<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> 
<!--
CSS
============================================= -->
<link rel="stylesheet" href="<c:url value="/resources/css/linearicons.css"/>">
<link rel="stylesheet" href="<c:url value="/resources/css/font-awesome.min.css"/>">
<link rel="stylesheet" href="<c:url value="/resources/css/nice-select.css"/>">
<link rel="stylesheet" href="<c:url value="/resources/css/ion.rangeSlider.css"/>">
<link rel="stylesheet" href="<c:url value="/resources/css/ion.rangeSlider.skinFlat.css"/>">
<link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.css"/>">
<link rel="stylesheet" href="<c:url value="/resources/css/main.css"/>">
